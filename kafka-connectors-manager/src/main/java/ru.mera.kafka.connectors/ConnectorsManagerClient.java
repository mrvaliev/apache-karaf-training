package ru.mera.kafka.connectors;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.List;

/**
 * Initializes pre-defined connectors
 */
public class ConnectorsManagerClient {

    private static final String SERVICE_PATH = "/connectors";

    Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    ObjectMapper objectMapper;

    List<ConnectorConfig> connectorConfigs;

    String serverUrl;

    @PostConstruct
    public void createConnectors() {
        logger.info("POST CONSTRUCT");
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        try {
            for (ConnectorConfig connectorConfig : connectorConfigs) {
                String json = objectMapper.writeValueAsString(connectorConfig);
                sendCreateRequest(json);
            }
        } catch (JsonProcessingException e) {
            logger.error("Json marhsalling error:", e);
        }
    }


    private void sendCreateRequest(String data) {

        Client client = ClientBuilder.newBuilder().newClient();
        WebTarget target = client.target(serverUrl);
        target = target.path(SERVICE_PATH);
        Invocation.Builder builder = target.request(MediaType.APPLICATION_JSON_TYPE);
        Response response = builder.post(Entity.json(data));

        logger.info("REQUEST TO {0} DONE.", serverUrl);
        logger.info("RESPONSE: " + response.getStatusInfo().getReasonPhrase());
    }


    private String buildUrl() {
        String url = new StringBuilder(serverUrl).append(SERVICE_PATH).toString();
        return url;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    public void setObjectMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public void setConnectorConfigs(List<ConnectorConfig> connectorConfigs) {
        this.connectorConfigs = connectorConfigs;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }
}
