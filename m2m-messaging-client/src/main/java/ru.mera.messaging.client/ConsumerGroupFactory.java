package ru.mera.messaging.client;

import ru.mera.messaging.kafka.ConsumerGroup;

/**
 * Created by aivaliev on 15.12.2016.
 */
public class ConsumerGroupFactory {

    public static ConsumerGroup createKafkaConsumerGroup() {
        ConsumerGroup consumerGroup = new ConsumerGroup("localhost:9092", "test", "group1", 1, new LoggingHandler());
        return consumerGroup;
    }
}
