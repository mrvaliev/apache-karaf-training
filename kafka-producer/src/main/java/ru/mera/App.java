package ru.mera;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.mera.messaging.Producer;

/**
 * Hello world!
 */
public class App {

    static Logger logger = LoggerFactory.getLogger("Main Logger");

    public static void main(String[] args) {
        logger.info("STARTING PRODUCER");
        runProducer();
        logger.info("STOPPING PRODUCER");
    }


    public static void runProducer() {
        Producer producer = new Producer();
        for (int i = 0; i < 10; i++)
            producer.send("message" + i);

    }
}
