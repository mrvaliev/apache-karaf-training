package ru.mera.messaging.kafka;

import ru.mera.messaging.api.MessageHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aivaliev on 14.12.2016.
 */
public class ConsumerGroup {

    private final int numberOfConsumers;
    private final String groupId;
    private final String topic;
    private final String brokers;
    private List<Consumer> consumers;

    public ConsumerGroup(String brokers, String topic, String groupId, int numberOfConsumers, MessageHandler messageHandler) {
        this.brokers = brokers;
        this.topic = topic;
        this.groupId = groupId;
        this.numberOfConsumers = numberOfConsumers;
        consumers = new ArrayList<>(numberOfConsumers);
        for (int i = 0; i < this.numberOfConsumers; i++) {
            Consumer consumer = new Consumer(brokers, topic, groupId, messageHandler);
            consumers.add(consumer);
        }
    }

    public void start() {
        for (Consumer consumer : consumers) {
            Thread t = new Thread(consumer);
            t.start();
        }
    }


    public void stop() {
        for (Consumer consumer : consumers) {
            consumer.stop();
        }
    }

    /**
     * @return the numberOfConsumers
     */
    public int getNumberOfConsumers() {
        return numberOfConsumers;
    }

    /**
     * @return the groupId
     */
    public String getGroupId() {
        return groupId;
    }
}
