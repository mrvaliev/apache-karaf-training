package ru.mera.messaging.kafka;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.mera.messaging.api.Message;
import ru.mera.messaging.api.MessageHandler;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Properties;


public class Consumer implements Runnable {

    private KafkaConsumer<String, String> kafkaConsumer;

    private MessageHandler messageHandler;

    /**
     * The time, in milliseconds, spent waiting in poll if data is not available.
     */
    public static int POLL_TIMEOUT = 1000;

    Logger logger = LoggerFactory.getLogger(Consumer.class.getName());

    static final String DEFAULT_GROUP = "default_group";
    final static String DEFAULT_TOPIC = "default_topic";

    public Consumer(String brokers, String topic, String groupId, MessageHandler messageHandler) {
        this.messageHandler = messageHandler;
        Properties props = createConsumerConfig(brokers, groupId);
        kafkaConsumer = new KafkaConsumer<>(props);
        kafkaConsumer.subscribe(Arrays.asList(topic));
    }


    private Properties createConsumerConfig(String brokers, String groupId) {
        Properties props = new Properties();
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, org.apache.kafka.common.serialization.StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, org.apache.kafka.common.serialization.BytesDeserializer.class);
        props.put("bootstrap.servers", brokers);
        props.put("group.id", groupId);

        try {
            InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("kafka-consumer.properties");
            props.load(inputStream);
            inputStream.close();
        } catch (IOException e) {
            logger.error(e.toString());
        }
        return props;
    }

    public void stop() {
        if (kafkaConsumer != null) {
            kafkaConsumer.wakeup();
        }
    }


    private void start() {
        while (true) {
            ConsumerRecords<String, String> records = kafkaConsumer.poll(POLL_TIMEOUT);
            for (ConsumerRecord<String, String> record : records) {
                logger.info(String.format(
                        "KafkaMessage has been get offset = %d, key = %s, value = %s",
                        record.offset(),
                        record.key(),
                        record.value()));
                Message message = new Message(record.key(), record.value());
                messageHandler.handleMessage(message);
            }
        }
    }

    @Override
    public void run() {
        try {
            start();
        } catch (WakeupException ex) {
            logger.info("Consumer was interrupted");
        } finally {
            kafkaConsumer.close();
        }
    }
}
