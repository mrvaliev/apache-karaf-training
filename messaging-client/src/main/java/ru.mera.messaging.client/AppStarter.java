package ru.mera.messaging.client;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.mera.messaging.kafka.ConsumerGroup;

/**
 * Created by aivaliev on 15.12.2016.
 */
public class AppStarter implements BundleActivator {

    Logger logger = LoggerFactory.getLogger(ru.mera.messaging.kafka.AppStarter.class);

    ConsumerGroup consumerGroup;

    @Override
    public void start(BundleContext bundleContext) throws Exception {
        logger.info("STARTING Messaging-Client bundle");

        consumerGroup = ConsumerGroupFactory.createKafkaConsumerGroup();
        consumerGroup.start();
        logger.info("ACTIVE Messaging-Client bundle");
    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        logger.info("STOPPING Messaging-Client bundle");
        consumerGroup.stop();
        logger.info("STOPPED Messaging-Client bundle");
    }
}
