package ru.mera.messaging.api;

/**
 * The entity object is already abstraction. It should not be interface
 */
public class Message {

    private String key;
    private Object value;


    public Message(String key, Object value) {
        this.key = key;
        this.value = value;
    }


    public String getKey() {
        return key;
    }

    public Object getValue() {
        return value;
    }
}
